/*!  @file                 TClientHistorique.h
     @brief                Déclaration de TClientHistorique

 */
//** **************************************************************************

#ifndef TClientHistorique_h
#define TClientHistorique_h

#include "../ATachePeriodique/ATachePeriodique.h"
//! Gère les données moyennes de la dernière heure des capteurs et les envoie
//! sur Google Drive pour leur stockage à long terme
class TClientHistorique : public ATachePeriodique {
private:
    WiFiClient client;
    const char *host;
    const int httpPort;
    String Dev_id;

public:
    TClientHistorique(long periode); // Ajouter une période par défaut
    double humidite;
    double temp;

protected:
    int ExecuterPeriodique();
};

#endif
