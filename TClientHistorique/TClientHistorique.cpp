/*!  @file                 TClientHistorique.cpp
     @brief                Implémentation de TClientHistorique

 */
//** **************************************************************************

#include "TClientHistorique.h"

//** ***************************************************************************
/*!  @brief                Initialise une tâche pour envoyer l'historique
                           des capteurs toutes les heures au serveur Google
                           Drive

 */
TClientHistorique::TClientHistorique(
    //! Interval d'exécution de l'envoi de l'historique au serveur
    long periode) :
    ATachePeriodique(periode),
    httpPort(80), host("api.pushingbox.com") {
    Dev_id = "v617A17A3E747333";
    temp = 0.0;
    humidite = 0.0;
}

//** ***************************************************************************
/*!  @brief                Implémentation de
                           ATachePeriodique::ExecuterPeriodique
                           Envoie l'historique de la température de chaque
                           capteur au serveur pour le stockage à long terme

     @return               Un code d'erreur
     @retval 0             Aucune erreur

 */
int TClientHistorique::ExecuterPeriodique() {
    Serial.print("connecting to ");
    Serial.println(host);
    if (!client.connect(host, httpPort)) {
        Serial.println("connection failed");
        return 0;
    }
    String string_temperature = String(temp, DEC);
    String string_humidity = String(humidite, DEC);
    String url = "/pushingbox?devid=" + Dev_id +
                 "&temperature=" + string_temperature +
                 "&humidite=" + string_humidity;
    Serial.print("requesting URL: ");
    Serial.println(url);

    client.print(String("GET ") + url + " HTTP/1.1\r\n" + "Host: " + host +
                 "\r\n" + "Connection: close\r\n\r\n");

    Serial.println("request sent");
    bool lecturecomplete = false;
    while (lecturecomplete == false) {
        String line = client.readStringUntil('\n');
        if (line == "\r") {
            Serial.println("headers received");
            lecturecomplete = true;
        }
    }
    String line = client.readStringUntil('\n');
    if (line.startsWith("{\"state\":\"success\"")) {
        Serial.println("esp8266/Arduino CI successfull!");
    } else {
        Serial.println("esp8266/Arduino CI has failed");
    }
    Serial.println("reply was:");
    Serial.println("==========");
    Serial.println(line);
    Serial.println("==========");
    Serial.println("closing connection");
}
