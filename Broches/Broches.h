/*!  @file                 Broches.h
     @brief                Déclaration des broches associées aux différentes
                           fonctions

 */
//** **************************************************************************

#ifndef Broches_h
#define Broches_h

// Définition des broches
#include <Arduino.h>

//! Interrupteur de sélection du mode (automatique/manuel)
static const int SW_MODE = D0;
//! Niveau logique de SW_MODE en mode manuel
#define MODE_MANUEL HIGH
//! Niveau logique de SW_MODE en mode automatique
#define MODE_AUTOMATIQUE LOW

//! Relais pour le contrôle de la direction des moteurs
static const int DIRECTION_MOTEURS[2] = {D9, D10};
//! Broche de l'interrupteur de direction de moteurs permettant de savoir s'il
//! faut ouvrir le côté ouvrant
static const int SW_DIRECTION_OUVRIR = D2;
//! Broche de l'interrupteur de direction de moteurs permettant de savoir s'il
//! faut fermer le côté ouvrant
static const int SW_DIRECTION_FERMER = D3;
//! Niveau logique de SW_DIRECTION_OUVRIR ou SW_DIRECTION_FERMER lorsque la
//! direction est sélectionnée.
#define DIRECTION_SELECTIONNE LOW

//! Relais pour la sélection du moteur
static const int SELECTION_MOTEUR = D8;
//! Interrupteur pour le contrôle de la sélection du moteur en mode manuel
static const int SW_SELECTION_MOTEUR = D4;
//! Niveau logique de SW_SELECTION_MOTEUR lorsque le moteur 0 est sélectionné
#define MOTEUR0_SELECTIONNE HIGH
//! Niveau logique de SW_SELECTION_MOTEUR lorsque le moteur 1 est sélectionné
#define MOTEUR1_SELECTIONNE LOW

//! Relais pour la commande du chauffage.
static const int CHAUFFAGE = D7;
//! Interrupteur pour la commande du chauffage en mode manuel.
static const int SW_CHAUFFAGE = D6;

//! Relais pour la commande de la ventilation
static const int VENTILATION = 10;
//! Interrupteur pour la commande de la ventilation en mode manuel.
static const int SW_VENTILATION = D5;

//! Niveau logique de SW_CHAUFFAGE ou SW_VENTILATION lorsqu'il faut allumer le
//! chauffage/ventilation
#define ALLUMER LOW
//! Niveau logique de SW_CHAUFFAGE ou SW_VENTILATION lorsqu'il faut éteindre le
//! chauffage/ventilation
#define ETEINDRE HIGH

//! Capteurs de température et d'humidité DHT22
static const int CAPTEURS[2] = {D0, D1};

#endif
