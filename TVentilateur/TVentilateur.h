/*!  @file                 TVentilateur.h
     @brief                Déclaration de TVentilateur

 */
//** **************************************************************************

#ifndef TVentilateur_h
#define TVentilateur_h

#include "../AActionneur/AActionneur.h"

//! Contrôle un ventilateur pour l'aération de la serre
class TVentilateur : public AActionneur<bool> {
public:
    //! Broche de contrôle du ventilateur
    int Broche;

    TVentilateur(int broche);

    using AActionneur<bool>::Etat;
    int Etat(bool etat);
};

#endif
