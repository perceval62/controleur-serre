/*!  @file                 TVentilateur.cpp
     @brief                Implémentation de TVentilateur

     @internal
     @todo                 L'implémentation est la même que pour la TFournaise,
                           on pourrait les combiner
 */
//** **************************************************************************

#include "TVentilateur.h"
// digitalWrite
#include <Arduino.h>

//! Niveau logique pour allumer le ventilateur
#define ALLUME 1
//! Niveau logique pour éteindre le ventilateur
#define ETEINT 0

//** ***************************************************************************
/*!  @brief                Initialise un ventilateur avec la broche donnée
                           et l'éteint

 */
TVentilateur::TVentilateur(
    //! Broche de contrôle du ventilateur
    int broche) {
    // Initialiser la broche à la broche donnée.
    Broche = broche;

    // Configurer la broche en sortie.
    pinMode(broche, OUTPUT);

    // Fermer le ventilateur.
    Etat(false);
}

//** ***************************************************************************
/*!  @brief                Implémentation de AActionneur::Etat(etat)
                           Modifie l'état du ventilateur

     @return               Un code d'erreur
     @retval 0             Aucune erreur

 */
int TVentilateur::Etat(
    //! Nouvel état du ventilateur
    bool etat) {
    // Changer l'état du ventilateur à celui donné.
    digitalWrite(Broche, etat ? ALLUME : ETEINT);
    FEtat = etat;

    // Retourner le code de réussite à la routine appelante.
    return 0;
}
