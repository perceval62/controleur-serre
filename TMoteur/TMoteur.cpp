/*!  @file                 TMoteur.cpp
     @brief                Implémentation de TMoteur

 */
//** **************************************************************************

#include "TMoteur.h"
#include <Arduino.h>

//! Pourcentage d'ouverture d'un côté ouvrant pas milliseconde
//! Représente la vitesse à laquelle un côté s'ouvre
#define PCT_PAR_MS 0.01
#define DELAI 5000

//! États des broches en fonction de la direction
static const int ETATS_BROCHES[][2] = {
    {0, 0}, // dmNeutre
    {0, 1}, // dmOuvrir
    {1, 0}, // dmFermer
};

//** ***************************************************************************
/*!  @brief                Initialise un moteur avec les broches de direction et
                           l'hystérésis donné. Le moteur est également déplacé à
                           la position 0.

 */
TMoteur::TMoteur(
    //! Plage de position dans laquelle doit se trouver le moteur pour s'arrêter
    double hysteresis,
    //! Broche de contrôle de la direction du moteur
    const int *brochesDirection) {
    // Initialiser l'hystérésis à celui donné.
    Hysteresis = hysteresis;

    // Initialiser la commande à 0.
    FCommande = 0.0;

    // Pour les 2 broches
    for (int i = 0; i < 2; i++) {
        int broche = brochesDirection[i];

        // Assigner la broche à celle de l'objet.
        Broches[i] = broche;

        // Configurer la broche en sortie.
        pinMode(broche, OUTPUT);
    }

    // Arrêter le déplacement des moteurs.
    FEtat = dmOuvrir; // Cette ligne est nécessaire, puisque si FEtat est déjà à
                      // dmNeutre, il ne changera pas son état et les niveaux
                      // logiques des broches puisqu'il croira qu'il n'y a rien
                      // à faire.
    Etat(dmNeutre);
}

//** ***************************************************************************
/*!  @brief                Déplace le moteur à la position donnée et bloque le
                           programme tant que le moteur n'y est pas rendu

 */
void TMoteur::Reinitialiser(
    //! Position où déplacer le moteur
    double position) {
}

//** ***************************************************************************
/*!  @brief                Calcul la position du moteur

     @return               La position du moteur entre 0.0 et 100.0%

 */
double TMoteur::Position() {
    // Initialiser la position au début du déplacement dans la direction.
    double pos = FPositionDebut;

    // Si le moteur est en mouvement
    if (Etat() != dmNeutre) {
        // Calculer la différence de position depuis le début du déplacement.
        TTemps tempsPasse = millis() - FDebutDirection;
        double difference = tempsPasse * PCT_PAR_MS;

        // Ajouter cette différence à la position.
        if (Etat() == dmOuvrir)
            pos += difference;
        else if (Etat() == dmFermer)
            pos -= difference;

        // Restreindre la position entre 0 et 100%.
        if (pos < 0.0)
            pos = 0.0;
        else if (pos > 100.0)
            pos = 100.0;
    }

    return pos;
}

//** ***************************************************************************
/*!  @brief                Obtenir la position où le moteur se dirige

     @return               La position où le moteur se dirige

 */
double TMoteur::Commande() {
    return FCommande;
}

//** ***************************************************************************
/*!  @brief                Changer la commande du moteur

     @return               Un code d'erreur
     @retval 0             Aucune erreur
     @retval 1             La commande n'est pas entre 0.0 et 100.0

 */
int TMoteur::Commande(
    //! Position où diriger le moteur
    double commande) {
    int erreur = 1;
    if ((commande >= 0.0) && (commande <= 100.0)) {
        FCommande = commande;
        Etat(dmNeutre);
        if (Position() > commande) {
            Etat(dmFermer);
        } else if (Position() < commande) {
            Etat(dmOuvrir);
        }
        erreur = 0;
    }
    return erreur;
}

//** ***************************************************************************
/*!  @brief                Implémentation de int AActionneur::Etat(TEtat)
                           Initialise un contrôleur de direction avec les
                           broches données et l'état à neutre

     @return               Un code d'erreur
     @retval 0             Aucune erreur
     @retval 1             Direction donnée invalide

 */
int TMoteur::Etat(
    //! Direction du moteur
    TDirectionMoteur direction) {
    // Initialiser le code d'erreur au code de direction invalide.
    int erreur = 1;

    // Si la direction est valide.
    if (direction >= dmPremier && direction <= dmDernier) {
        // Si on veut passer de ouvrir à fermer ou de fermer à ouvrir sans
        // passer par neutre
        if (direction != FEtat && direction != dmNeutre && FEtat != dmNeutre)
            // Obliger le passage par l'état neutre.
            direction = dmNeutre;

        // Si la direction est différente
        if (direction != FEtat) {
            // Si le délai est passé ou que la direction est neutre
            if (millis() - FDebutDirection >= DELAI || direction == dmNeutre ||
                direction == FDirectionPrecedente) {
                // Obtenir l'état des broches en fonction de la direction.
                const int *etats = ETATS_BROCHES[direction];

                // Assigner l'état des broches aux broches.
                digitalWrite(Broches[0], etats[0]);
                digitalWrite(Broches[1], etats[1]);

                // Calculer la position actuelle et l'assigner à la position du
                // début du mouvement.
                FPositionDebut = Position();

                // Assigner l'heure actuelle au début de la direction.
                FDebutDirection = millis();

                // Assigner l'état à la direction précédente.
                FDirectionPrecedente = FEtat;

                // Assigner la direciton à l'état.
                FEtat = direction;
            }
        }

        // Assigner le code de réussite au code d'erreur.
        erreur = 0;
    }

    return erreur;
}

//** ***************************************************************************
/*!  @brief                Implémentation de ITache::Executer()
                           Vérifie si le moteur a atteint sa destination et si
                           oui, arrête le moteur, sinon dirige le moteur dans la
                           bonne direction pour s'y rendre

     @return               Un code d'erreur
     @retval 0             Aucune erreur
     @retval 1             Trop élevée
     @retval 2             Trop basse

 */
int TMoteur::Executer() {
    int codeErreur = 0;

    // Calculer la position du moteur.
    double pos = Position();

    // Si le moteur n'a pas atteint sa position
    // i.e. Si la position n'est pas à la commande ou
    // (la commande est entre 0 et 100 exclusivement et qu'elle
    // est en dehors de l'hystérésis autour de la commande)
    if (FCommande != pos ||
        ((FCommande > 0.0 && FCommande < 100.0) &&
         (pos > FCommande + Hysteresis || pos < FCommande - Hysteresis))) {
        if (pos > FCommande) {
            Etat(dmNeutre);
            Etat(dmFermer);
            codeErreur = 1;
        } else if (pos < FCommande) {
            Etat(dmNeutre);
            Etat(dmOuvrir);
            codeErreur = 2;
        }
    } else {
        Etat(dmNeutre);
    }

    return codeErreur;
}
