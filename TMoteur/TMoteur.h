/*!  @file                 TMoteur.h
     @brief                Déclaration de TMoteur et TDirectionMoteur

 */
//** **************************************************************************

#ifndef TMoteur_h
#define TMoteur_h

#include "../AActionneur/AActionneur.h"
#include "../ITache/ITache.h"
#include "../Temps/Temps.h"

//! Direction d'un moteur
enum TDirectionMoteur {
    //! Le moteur n'est pas en marche
    dmNeutre,
    //! Le moteur ouvre le côté ouvrant
    dmOuvrir,
    //! Le moteur ferme le côté ouvrant
    dmFermer,

    dmPremier = dmNeutre,
    dmDernier = dmFermer,
    dmDefaut = dmNeutre,
};

//! Gère un moteur et son déplacement vers une position
class TMoteur : public ITache, public AActionneur<TDirectionMoteur> {
private:
    //! Heure à laquelle le moteur a commencé à aller dans cette direction
    TTemps FDebutDirection;
    //! Position du moteur quand il a commencé à aller dans cette direction
    double FPositionDebut;

    //! Position à laquelle le moteur doit se rendre
    double FCommande;

    TDirectionMoteur FDirectionPrecedente;

public:
    //! Marge dans laquelle peut se trouver la position du moteur pour qu'il
    //! soit arrêté
    double Hysteresis;

    //! Broches pour la sélection de la direction
    int Broches[2];

    TMoteur(double hysteresis, const int *brochesDirection);

    void Reinitialiser(double position);

    double Position();

    double Commande();
    int Commande(double commande);

    using AActionneur<TDirectionMoteur>::Etat;
    int Etat(TDirectionMoteur direction);

    int Executer();
};
#endif
