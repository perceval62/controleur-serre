/*!  @file                 ITache.h
     @brief                Déclaration de ITache

 */
//** **************************************************************************

#ifndef ITache_h
#define ITache_h

//! Interface permettant l'éxécution d'une tâche
class ITache {
    //*************************************************************************
    /*!  @brief                Exécute la tâche

         @return               Un code d'erreur
         @retval 0             Aucune erreur
         @retval autre         Voir l'erreur de l'implémentation

     */
    virtual int Executer() = 0;
};

#endif
