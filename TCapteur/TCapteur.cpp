/*!  @file                 TCapteur.cpp
     @brief                Implémentation de TCapteur

 */
//** **************************************************************************

#include "TCapteur.h"

// INFINITY
#include <math.h>

//! Modèle du capteur
#define TYPE_CAPTEUR DHTesp::DHT22

//==============================================================================
// Implémentation de TSommaireMesures
//==============================================================================

//** ***************************************************************************
/*!  @brief                Initialise Min à INFINITY, MAX à -INFINITY et Moyenne
                           à 0

 */
TSommaireMesures::TSommaireMesures() {
    Min = INFINITY;
    Max = -INFINITY;
    Moyenne = 0.0;
}

//** ***************************************************************************
/*!  @brief                Calcul le minimum, le maximum, et la moyenne de la
                           liste des mesures données et les met dans ses
                           propriétés

     @note                 Si la taille de mesures est 0, Min, Max et Moyenne ne
                           seront pas des nombres réels. Voir le constructeur
                           par défaut pour leurs valeurs.

 */
TSommaireMesures::TSommaireMesures(
    //! Mesures à analyser
    TMesureCapteur *mesures,
    //! Nombre de mesures à analyser
    unsigned int nbMesures) :
    TSommaireMesures() {
    // Pour chaque mesure
    for (int i = 0; i < nbMesures; i++, mesures++) {
        // Ajouter la mesure à la moyenne.
        Moyenne += *mesures;

        // Assigner la mesure à la valeur minimale si elle est inférieure à
        // celle-ci.
        if (*mesures < Min)
            Min = *mesures;

        // Assigner la mesure à la valeur maximale si elle est supérieure à
        // celle-ci.
        if (*mesures > Max)
            Max = *mesures;
    }

    // Diviser la somme des mesures par leur nombre pour obtenir la moyenne des
    // mesures.
    Moyenne /= nbMesures;
}

//** ***************************************************************************
/*!  @brief                Calcul la moyenne des moyennes, le maximum
                           des maximums et le minimum des minimums de tous les
                           sommaires

     @note                 Si la taille de mesures est 0, Min, Max et Moyenne ne
                           seront pas des nombres réels. Voir le constructeur
                           par défaut pour leurs valeurs.

 */
TSommaireMesures::TSommaireMesures(
    //! Liste des sommaires de mesures
    TSommaireMesures *sommaires,
    //! Nombre de sommaires dans la liste
    unsigned int nbSommaires) :
    TSommaireMesures() {
    // Pour chaque sommaire
    for (int i = 0; i < nbSommaires; i++, sommaires++) {
        // Ajouter la moyenne du sommaire à la moyenne.
        Moyenne += sommaires->Moyenne;

        // Assigner le minimum du sommaire à la valeur minimale si elle est
        // inférieure à celle-ci.
        if (sommaires->Min < Min)
            Min = sommaires->Min;

        // Assigner le maximum du sommaire à la valeur maximale si elle est
        // supérieure à celle-ci.
        if (sommaires->Max > Max)
            Max = sommaires->Max;
    }

    // Diviser la somme des mesures par leur nombre pour obtenir la moyenne des
    // mesures.
    Moyenne /= nbSommaires;
}

//==============================================================================
// Implémentation de TSommaireMesures
//==============================================================================

//** ***************************************************************************
/*!  @brief                Initialise un historique de mesure sans aucune mesure

 */
TMesuresHistoriques::TMesuresHistoriques() {
    Reinitialiser();
}

//** ***************************************************************************
/*!  @brief                Calcul la moyenne de l'historique

 */
TMesureCapteur TMesuresHistoriques::Moyenne() {
    return FSommeMesures / FNbMesures;
}

//** ***************************************************************************
/*!  @brief                Ajoute une mesure à la somme de mesures et incrémente
                           le nb de mesures

 */
void TMesuresHistoriques::Ajouter(
    //! Mesure à ajouter
    TMesureCapteur mesure) {
    // Incrémenter le nb de mesures.
    FNbMesures++;

    // Ajouter la mesure à la somme de mesures.
    FSommeMesures += mesure;
}

//** ***************************************************************************
/*!  @brief                Supprime toutes les mesures de l'historique

 */
void TMesuresHistoriques::Reinitialiser() {
    FNbMesures = 0;
    FSommeMesures = 0.0;
}

//** ***************************************************************************
/*!  @brief                Nombre de mesures dans l'historique

 */
unsigned int TMesuresHistoriques::NbMesures() {
    return FNbMesures;
}

//==============================================================================
// Implémentation de TCapteur
//==============================================================================

//** ***************************************************************************
/*!  @brief                Initialise un capteur DHT22 sur la broche D0
                           compatible avec I²C

 */
TCapteur::TCapteur() {
}

//** ***************************************************************************
/*!  @brief                Initialise un capteur DHT22 sur la broche compatible
                           avec I²C

 */
TCapteur::TCapteur(
    //! Broche du capteur
    int broche) {
    Capteur.setup(broche, TYPE_CAPTEUR);
}

//** ***************************************************************************
/*!  @brief                Lit la température et l'humidité du capteur et
                           l'ajoute à ses mesures

     @return               Un code d'erreur
     @retval 0             Aucune erreur
     @retval 1             La température n'a pas pu être lue
     @retval 2             L'humidité n'a pas pu être lue
     @retval 3             La température et l'humidité n'ont pas pues être lues

 */
int TCapteur::Lire() {
    int codeErreur = 0;

    // Lire la température et l'ajouter aux mesures de température.
    TMesureCapteur temperature = Capteur.getTemperature();
    if (isnan(temperature)) {
        codeErreur = 1;
    } else {
        TemperatureCourtTerme.Ajouter(temperature);
        TemperatureHistorique.Ajouter(temperature);
    }

    // Lire l'humidité et l'ajouter aux mesures d'humidité.
    TMesureCapteur humidite = Capteur.getHumidity();
    if (isnan(humidite)) {
        codeErreur += 2;
    } else {
        HumiditeCourtTerme.Ajouter(humidite);
        HumiditeHistorique.Ajouter(humidite);
    }

    return codeErreur;
}

//** ***************************************************************************
/*!  @brief                Réinitialise l'historique de température et
                           d'humidité

 */
void TCapteur::ReinitialiserHistorique() {
    TemperatureHistorique.Reinitialiser();
    HumiditeHistorique.Reinitialiser();
}
