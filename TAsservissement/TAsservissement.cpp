/*!  @file                 TAsservissement.cpp
     @brief                Implémentation de TAsservissement

 */
//** **************************************************************************

#include "TAsservissement.h"
#include "../TConsigne/TConsigne.h"
#include "../Temps/Temps.h"

//** ***************************************************************************
/*!  @brief                Initialise la tâche d'assevissement de la serre
                           avec une période de 5 minutes

 */
TAsservissement::TAsservissement(
    //! Interval d'exécution de l'asservissement
    long periode) :
    ATachePeriodique(periode) {
    FCommande = 0;
}

//** ***************************************************************************
/*!  @brief                Implémentation de
                           ATachePeriodique::ExecuterPeriodique
                           Assigne des consignes aux actionneurs afin d'asservir
                           la température

     @return               Un code d'erreur
     @retval 0             Aucune erreur
     @retval 1             Cible Prejour
     @retval 2             Cible Jour
     @retval 3             Cible prenuit
     @retval 4             Cible Nuit

 */
int TAsservissement::ExecuterPeriodique() {
    // Mettre un Executer() qui est executer en loop pour vérifier la position
    // des moteurs (ne fait pas partie du code)
    int CodeErreur = 0;
    time_t temps = temps();
    tm *p_tm = localtime(&temps);
    // Si l'heure est
    if (p_tm->tm_hour > 0 && p_tm->tm_hour < 5) {
        CodeErreur = 1;
        ChangerTemperature(Cible.Prejour);
    }
    // Si l'heure est
    else if (p_tm->tm_hour > 5 && p_tm->tm_hour < 12) {
        CodeErreur = 2;
        ChangerTemperature(Cible.Jour);
    }
    // Si l'heure est
    else if (p_tm->tm_hour > 12 && p_tm->tm_hour < 17) {
        CodeErreur = 3;
        ChangerTemperature(Cible.Prenuit);
    }
    // Si l'heure est
    else if (p_tm->tm_hour > 17 && p_tm->tm_hour < 0) {
        CodeErreur = 4;
        ChangerTemperature(Cible.Nuit);
    }
    return CodeErreur;
}

//** ***************************************************************************
/*!  @brief                Contrôle le ventilateur,
                           la fournaise et les moteurs
                           pour maintenir la température de la consigne

     @param   Consigne     Consigne de l'asservisement
     @return               Un code d'erreur
     @retval 0             Aucune erreur
     @retval 1             température critique
 */

int TAsservissement::ChangerTemperature(TConsigne Consigne) {
    int CodeErreur = 0;
    MotorSelection->Etat(0);
    // Coerce value to max or min
    if (FCommande < 0)
        FCommande = 0;
    else if (FCommande > 100)
        FCommande = 100;
    //
    if (Temperature.Moyenne < Consigne.TempMinCritique &&
        Temperature.Moyenne > Consigne.TempMaxCritique)
        CodeErreur = 1;
    else {
        if (Temperature.Moyenne <= Consigne.TempCible + Consigne.Hysteresis &&
            Temperature.Moyenne >= Consigne.TempCible - Consigne.Hysteresis) {
            Heater->Etat(ETEINT);
            Fan->Etat(ETEINT);
        } else if (Temperature.Moyenne > Consigne.TempCible) {
            if (FCommande < 100)
                FCommande += 10;
            Heater->Etat(ALLUME);
            Fan->Etat(ETEINT);
            MotorSelection->Moteurs[0].Commande(FCommande);
            MotorSelection->Moteurs[1].Commande(FCommande);
        } else if (Temperature.Moyenne < Consigne.TempCible) {
            if (FCommande > 0)
                FCommande -= 10;
            Heater->Etat(ETEINT);
            Fan->Etat(ALLUME);
            MotorSelection->Moteurs[0].Commande(FCommande);
            MotorSelection->Moteurs[1].Commande(FCommande);
        }
    }
    return CodeErreur;
}
