/*!  @file                 TAsservissement.h
     @brief                Déclaration de TAsservissement

 */
//** **************************************************************************

#ifndef TAsservissement_h
#define TAsservissement_h

#include "../ATachePeriodique/ATachePeriodique.h"
#include "../TCapteur/TCapteur.h"
#include "../TFournaise/TFournaise.h"
#include "../TJour/TJour.h"
#include "../TSelectionMoteur/TSelectionMoteur.h"
#include "../TVentilateur/TVentilateur.h"

//! Niveau logique pour allumer le ventilateur ou la fournaise
#define ALLUME 1
//! Niveau logique pour éteindre le ventilateur ou la fournaise
#define ETEINT 0

//! Asservit la température dans la serre selon une consigne en contrôlant les
//! actionneurs
class TAsservissement : public ATachePeriodique {
public:
    TAsservissement(
        long periode); // TODO ajouter une valeur par défaut pour période
    TSommaireMesures Temperature;
    TFournaise *Heater;
    TVentilateur *Fan;
    TSelectionMoteur *MotorSelection;
    TJour Cible;

protected:
    int ExecuterPeriodique();

private:
    double FCommande;
    int ChangerTemperature(TConsigne Consigne);
};

#endif
