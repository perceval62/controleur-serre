/*!  @file                 TJour.h
     @brief                Déclaration de TJour

 */
//** **************************************************************************

#ifndef TJour_h
#define TJour_h

#include "../TConsigne/TConsigne.h"

//! Contient les consignes pour les différentes personnes d'une journée
struct TJour {
    //! Consigne avant le levé du soleil
    TConsigne Prejour;
    //! Consigne pendant le jour
    TConsigne Jour;
    //! Consigne au levé du soleil
    TConsigne Prenuit;
    //! Consigne pendant la nuit
    TConsigne Nuit;
};

#endif
