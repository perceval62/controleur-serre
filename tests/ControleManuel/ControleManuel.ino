// #include "../../Broches/Broches.h"
//! ETATS_BROCHES, TDirectionMoteur
// #include "../../TMoteur/TMoteur.cpp"

//! Liste des broches à tester
static const int BROCHES[] = {
    DIRECTION_MOTEURS[0], DIRECTION_MOTEURS[1], SELECTION_MOTEUR, CHAUFFAGE,
    VENTILATION,
};

//! Niveau logique d'un ventilateur éteint
#define ETEINT 0

//! Délai de changement de direction des moteurs en ms
static const int DELAI_DIRECTION = 2000;
//! Direction précédente du moteur
TDirectionMoteur directionPrecedente = (TDirectionMoteur)-1;
//! Heure de début de la direction du moteur
long debutDirection = 0;

void setup() {
    //==========================================================================
    // Configurer les sorties en sorties et les éteindre.
    //==========================================================================
    for (int broche : BROCHES) {
        pinMode(broche, OUTPUT);
        digitalWrite(broche, ETEINT);
    }

    //==========================================================================
    // Configurer les entrées en entrées.
    //==========================================================================
    const int entrees[] = {
        SW_MODE,
        SW_DIRECTION_OUVRIR,
        SW_DIRECTION_FERMER,
        SW_SELECTION_MOTEUR,
        SW_CHAUFFAGE,
        SW_VENTILATION,
    };
    for (int entree : entrees)
        pinMode(entree, INPUT);
}

void loop() {
    // Sélectionner le moteur en fonction de l'état de l'interrupteur de
    // sélection de moteur.
    int indexMoteur = digitalRead(SW_SELECTION_MOTEUR) == MOTEUR1_SELECTIONNE;
    digitalWrite(SELECTION_MOTEUR, indexMoteur);

    // Obtenir la direction de moteurs sélectionnée.
    TDirectionMoteur direction = dmNeutre;
    if (digitalRead(SW_DIRECTION_OUVRIR) == DIRECTION_SELECTIONNE)
        direction = dmOuvrir;
    else if (digitalRead(SW_DIRECTION_FERMER) == DIRECTION_SELECTIONNE)
        direction = dmFermer;

    // Si la direction a changée et que le temps.
    long tempsDirection = millis() - debutDirection;
    if (direction != directionPrecedente && tempsDirection >= DELAI_DIRECTION) {
        // Changer la direction des moteurs.
        const int *etatDirection = ETATS_BROCHES[direction];
        for (int i = 0; i < 2; i++)
            digitalWrite(DIRECTION_MOTEURS[i], etatDirection[i]);

        // Assigner les données de cette direction à celle de la direction
        // précédente.
        directionPrecedente = direction;
        debutDirection = millis();
    }

    // Changer l'état du ventilateur en fonction de l'état de l'interrupteur
    // de ventilation.
    int ventilateurAllume = digitalRead(SW_VENTILATION) == ALLUMER;
    digitalWrite(VENTILATION, ventilateurAllume);

    // Changer l'état de la fournaise en fonction de l'état de
    // l'interrupteur de chauffage.
    int etatFournaise = digitalRead(SW_CHAUFFAGE) == ALLUMER;
    digitalWrite(
        CHAUFFAGE,
        !etatFournaise); // TODO retirer l'inversement pour le modèle final.
}
