/*!  @file                 TClientNotifications.h
     @brief                Déclaration de TClientNotifications

     @internal

     @todo                 Faire une classe abstraite de client, car je pense
                           que beaucoup de code va se répéter
 */
//** **************************************************************************

#ifndef TClientNotifications_h
#define TClientNotifications_h

#include "../ATachePeriodique/ATachePeriodique.h"

//! Initialise le client avec la période donnée pour les vérifications
class TClientNotifications : public ATachePeriodique {
private:
    // WiFiClient FClientWifi;
    // HttpClient FClientHttp;

public:
    TClientNotifications(long periode); // TODO période par défaut

protected:
    int ExecuterPeriodique();
};
#endif
