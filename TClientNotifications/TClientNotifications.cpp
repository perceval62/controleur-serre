/*!  @file                 TClientNotifications.cpp
     @brief                Implémentation de TClientNotifications

 */
//** **************************************************************************

#include "TClientNotifications.h"

//** ***************************************************************************
/*!  @brief                Initialise le client avec la période donnée pour les
                           vérifications

     @return               Un code d'erreur
     @retval 0             Aucune erreur

 */
TClientNotifications::TClientNotifications(
    //! Interval de vérification du dépassement des seuils critiques
    long periode) :
    ATachePeriodique(periode) {
}

//** ***************************************************************************
/*!  @brief                Implémentation de
                           ATachePeriodique::ExecuterPeriodique
                           Traite l'exécution des serveurs

     @return               Un code d'erreur
     @retval 0             Aucune erreur

 */
int TClientNotifications::ExecuterPeriodique() {
}
