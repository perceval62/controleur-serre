/*!  @file                 TServeur.cpp
     @brief                Implémentation de TServeur

 */
//** **************************************************************************

#include "TServeur.h"
#include "Cles.h"

//! Mot de passe pour accéder au serveur
//! TODO changer ce mot de passe en production
#define MDP_SERVEUR "Changer ce mot de passe!"

//! Chaîne de caractères pour une capteur dont l'unité le degrés Celsius
static const char *UNITE_CELSIUS = "C";
//! Chaîne de caractères pour une capteur dont l'unité l'humidité relative en
//! pourcentage
static const char *UNITE_HUMIDITE_RELATIVE = "HR";

//=============================================================================
// Types d'actionneurs
//=============================================================================
//! Nom du type d'un moteur
static const char *TYPE_MOTEUR = "moteur";
//! Nom du type d'un ventilateur
static const char *TYPE_VENTILATION = "ventilation";
//! Nom du type d'une fournaise
static const char *TYPE_CHAUFFAGE = "chauffage";

//=============================================================================
// Noms des actionneurs
//=============================================================================
//! Nom du type d'un moteur
static const char *NOMS_MOTEURS[NB_MOTEURS] = {"Côté ouvrant Est",
                                               "Côté ouvrant Ouest"};
//! Nom du type d'un ventilateur
static const char *NOM_VENTILATEUR = "Ventilateur";
//! Nom du type d'une fournaise
static const char *NOM_FOURNAISE = "Fournaise";

//=============================================================================
// États des actionneurs formattés
//=============================================================================
//! Noms des directions de moteur
static const char *DIRECTION_MOTEUR[] = {"neutre", "ouvrir", "fermer"};

//=============================================================================
// Codes d'erreurs HTTP
//=============================================================================
//! Code d'erreur interne du serveur
static const int CODE_ERREUR_INTERNE = 500;
//! Code de méthode invalide
static const int CODE_METHODE_INVALIDE = 405;
//! Code d'erreur d'autorisation, le client a entré le mauvais mot de passe
//! pour accéder au serveur
static const int CODE_NON_AUTHORISE = 401;
//! Code de réussite avec contenu
static const int CODE_OK = 200;
//! Code de réussite sans contenu
static const int CODE_AUCUN_CONTENU = 204;

//=============================================================================
// Constantes pour l'authentification
//=============================================================================
//! Entête HTTP pour l'authentification
static const char *ENTETE_AUTHENTIFICATION = "Authorization";
//! Schéma d'authentification
static const char *SCHEMA_AUTHENTIFICATION = "Basic ";
//! Schéma d'authentification
static const int TAILLE_SCHEMA_AUTHENTIFICATION = 6;

static ESP8266WebServerSecure Serveur(443);
static WiFiClient Client;
static StaticJsonDocument<1024> doc;

//** ***************************************************************************
/*!  @brief                Implémentation de ITache::Executer
                           Traite une requête au serveur HTTP s'il y en a une

     @return               Un code d'erreur
     @retval 0             Aucune erreur

 */
int TServeur::Executer() {
    Serveur.handleClient();
}

StaticJsonDocument<1024> &LectureServeur() {
    deserializeJson(doc, Client);
    return doc;
}

//** ***************************************************************************
/*!  @brief                Vérifie si le client a l'autorisation d'accéder au
                           serveur et lui envoie une erreur dans le cas
                           contraire.

     @return               Un code d'erreur
     @retval 0             Le client a l'autorisation d'accéder au serveur.
     @retval 1             Le client n'a pas le droit d'accéder au serveur.

 */
int Authentifier() {
    // Initialiser le code d'erreur au code d'accès non autorisé.
    int codeErreur = 1;

    // Obtenir la valeur de l'entête authentification
    String valeur = Serveur.header(ENTETE_AUTHENTIFICATION);

    // Si le schéma d'authentification est le bon
    if (valeur.startsWith(SCHEMA_AUTHENTIFICATION)) {
        // Obtenir le mot de passe à partir de la valeur.
        // i.e. Sauter le nom du schéma d'authenification.
        const char *mdp = &valeur.c_str()[TAILLE_SCHEMA_AUTHENTIFICATION];

        // Si le mot de passe donné est le même que le mot de passe du serveur.
        if (strcmp(mdp, MDP_SERVEUR) == 0)
            // Assigner le code d'accès autorisé au code d'erreur.
            codeErreur = 0;
    }

    // Si l'accès n'est pas autorisé
    if (codeErreur != 0) {
        // Retourner le code d'accès non autorisé au client.
        Serveur.send(CODE_NON_AUTHORISE, NULL);
    }

    return codeErreur;
}

//** ***************************************************************************
/*!  @brief                Envoi le document sérialisé au client du serveur avec
                           le code 200

     @return               Un code d'erreur
     @retval 0             Aucune erreur
     @retval 1             Le document n'a pas pu être sérialisé

 */
int EnvoyerDocument() {
    // Initialiser le code d'erreur au code de réussite.
    int codeErreur = 0;

    // Sérialiser le document.
    // Si le document n'a pas pu être sérialisé
    String serialise;
    if (serializeJson(doc, serialise) > 0) {
        // Envoyer le document sérialisé.
        Serveur.send(CODE_OK, "application/json", serialise);
    }
    // Sinon
    else {
        // Assigner le code d'erreur de sérialisation au code d'erreur.
        codeErreur = 1;

        // Envoyer un code d'erreur interne au serveur.
        Serveur.send(CODE_ERREUR_INTERNE, NULL);
    }

    return codeErreur;
}

/*bool RequeteServeur() {
    Client = Serveur.client();
    if (false) {                       // If a new client connects,
        Serial.println("New Client."); // print a message out in the serial port
        String requete =
            ""; // make a String to hold incoming data from the client
        while (Client.connected()) { // loop while the client's connected
            if (Client
                    .available()) { // if there's bytes to read from the client,
                String currentLine =
                    Client.readStringUntil('\r'); // read a byte, then
                Serial.println(requete);
                char c = '\0';
                if (c == '\n') { // if the byte is a newline character
                    // if the current line is blank, you got two newline
                    // characters in a row. that's the end of the client HTTP
                    // request, so send a response:
                    if (currentLine.length() == 0) {
                        // HTTP headers always start with a response code (e.g.
                        // HTTP/1.1 200 OK) and a content-type so the client
                        // knows what's coming, then a blank line:
                        // Serveur.HandleClient();
                    } else { // if you got a newline, then clear currentLine
                        currentLine = "";
                    }
                } else if (c != '\r') { // if you got anything else but a
                                        // carriage return character,
                    currentLine += c;   // add it to the end of the currentLine
                }
            }
        }
        // Close the connection
        Client.stop();
        Serial.println("Client disconnected.");
        Serial.println("");
    }
}*/

//** ***************************************************************************
/*!  @brief                Réponde au chemin "/capteur" du serveur

 */
void ServeurCertificat() {
    if (Serveur.method() == HTTP_GET) {
        Serveur.send(204);
    } else {
        Serveur.send(CODE_METHODE_INVALIDE, NULL);
    }
}

//** ***************************************************************************
/*!  @brief                Insère un objet de capteur dans un JsonArray

 */
void InsererCapteur(
    //! Objet où insérer les données du capteur.
    JsonArray &array,
    //! Nom du capteur.
    String &nom,
    //! Mesure du capteur
    double mesure,
    //! Unité du capteur
    const char *unite) {
    // Créer un nouvel objet dans l'array.
    JsonObject obj = array.createNestedObject();

    // Ajouter le nom du capteur.
    obj["nom"] = nom;

    // Ajouter la mesure du capteur.
    obj["mesure"] = mesure;

    // Écrirue l'unité dans l'array.
    obj["unite"] = unite;
}
//** ***************************************************************************
/*!  @brief                Réponde au chemin "/capteur" du serveur

 */
void ServeurCapteurs() {
    // Quitter la fonction si le client n'a pas l'authorisation.
    if (Authentifier() != 0)
        return;

    if (Serveur.method() == HTTP_GET) {
        doc.clear();
        JsonArray array = doc.to<JsonArray>();

        // Pour chaque capteur.
        int i = 0;
        for (TCapteur &capteur : groupeCapteurs.Capteurs) {
            // Créer un nouvel objet pour la température.
            InsererCapteur(array, String("Température ") + i,
                           capteur.TemperatureCourtTerme.Sommaire().Moyenne,
                           UNITE_CELSIUS);

            // Créer un nouvel objet pour l'humidité.
            InsererCapteur(array, String("Humidité ") + i,
                           capteur.HumiditeCourtTerme.Sommaire().Moyenne,
                           UNITE_HUMIDITE_RELATIVE);

            // Incrémenter l'index du capteur.
            i++;
        }

        // Envoyer le document.
        EnvoyerDocument();
    } else {
        Serveur.send(CODE_METHODE_INVALIDE, NULL);
    }
}

//** ***************************************************************************
/*!  @brief                Insère un objet d'actionneur dans un JsonArray

 */
JsonObject InsererActionneur(
    //! Objet où insérer l'actionneur
    JsonArray &array,
    //! Type de l'actionneur
    const char *type,
    //! Nom de l'actionneur
    const char *nom,
    //! État de l'actionneur
    String etat) {
    // Créer un nouvel objet dans l'array.
    JsonObject obj = array.createNestedObject();

    // Ajouter le type de l'actionneur.
    obj["type"] = type;

    // Ajouter le nom de l'actionneur.
    obj["nom"] = nom;

    // Ajouter l'état de l'actionneur.
    obj["etat"] = etat;

    return obj;
}
//** ***************************************************************************
/*!  @brief                Insère un objet de jour dans un JsonArray

 */
JsonObject InsererJour(
    //! Objet où insérer le jour
    JsonArray &array,
    //! Type de jour
    const char *nom,
    //! Consigne de prejour
    double prejour,
    //! Consigne de jour
    double jour,
    //! Consigne de prenuit
    double prenuit,
    //! Consigne de nuit
    double nuit) {
    JsonObject Jour = array.createNestedObject();
    Jour["nom"] = nom;
    Jour["prejour"] = prejour;
    Jour["jour"] = jour;
    Jour["prenuit"] = prenuit;
    Jour["nuit"] = nuit;
    return Jour;
}
//** ***************************************************************************
/*!  @brief                Répond au chemin "/actionneurs" du serveur

 */
void ServeurActionneurs() {
    // Quitter la fonction si le client n'a pas l'authorisation.
    if (Authentifier() != 0)
        return;

    if (Serveur.method() == HTTP_GET) {
        doc.clear();
        JsonArray array = doc.to<JsonArray>();

        // Pour chaque moteur
        for (int i = 0; i < NB_MOTEURS; i++) {
            TMoteur &moteur = selectionMoteur.Moteurs[i];
            // Insérer l'actionneur dans l'array.
            JsonObject obj =
                InsererActionneur(array, TYPE_MOTEUR, NOMS_MOTEURS[i],
                                  String(DIRECTION_MOTEUR[moteur.Etat()]));

            // Ajouter la position du moteur à l'objet.
            obj["position"] = moteur.Position();
        }

        // Insérer le ventilateur dans l'array.
        InsererActionneur(array, TYPE_VENTILATION, NOM_VENTILATEUR,
                          String(ventilateur.Etat()));

        // Insérer le ventilateur dans l'array.
        InsererActionneur(array, TYPE_CHAUFFAGE, NOM_FOURNAISE,
                          String(fournaise.Etat()));

        // Envoyer le document.
        EnvoyerDocument();
    } else {
        Serveur.send(CODE_METHODE_INVALIDE, NULL);
    }
}

void ServeurAlerte() {
    if (Serveur.method() == HTTP_GET) {
        if (false) { // si alerte
            doc.clear();
            doc["titre"] = "";
            doc["message"] = "";
            serializeJson(doc, Client);
            Serveur.send(200, "text/plain", "Alerte");
        } else {
            Serveur.send(204, "text/plain", "Aucune alerte");
        }
    }
}

void ServeurTypeJour() {
    if (Serveur.method() == HTTP_GET) {
        static const char *typeJour = "ensoleille";
        Serveur.send(200, "text/plain", typeJour);
    }
}

void ServeurConsignes() {
    if (Serveur.method() == HTTP_GET) {
        doc.clear();
        JsonArray array = doc.to<JsonArray>();
        InsererJour(array, "Nom", 20.0, 20.0, 20.0, 20.0);
        InsererJour(array, "test", 20.0, 20.0, 20.0, 20.0);

        // Envoyer le document.
        EnvoyerDocument();
    } else if (Serveur.method() == HTTP_POST) {
        deserializeJson(doc, Client);
        // Changer les consigne de l'asservissement
    }
    Client.println("HTTP/1.1 200 OK");
    Client.println("Content-type: text/html");
    Client.println("Connection: close");
    Client.println();
}

void ServeurCycle() {
    if (Serveur.method() == HTTP_GET) {
        doc.clear();
        JsonObject object = doc.to<JsonObject>();
        JsonObject cycle = object.createNestedObject("cycle");
        cycle["repetitions"] = 0;
        cycle["duree_chauffage"] = 0;
        cycle["duree_ventilation"] = 0;
        JsonObject progression = object.createNestedObject("progression");
        progression["repetition"] = 0;
        progression["etape"] = "chauffage ou ventilation";
        progression["duree"] =
            " Durée depuis le début de l'étape dans cette répétition";
        serializeJson(doc, Client);
    } else if (Serveur.method() == HTTP_POST) {
        deserializeJson(doc, Client);
        // Changer les consigne de l'asservissement
    } else if (Serveur.method() == HTTP_DELETE) {
        // Vérifier l'éxécution d'un cycle l'arêter si il y'en a. Envoyer une
        // erreur si non
    }
}

//** ***************************************************************************
/*!  @brief                Initialise le serveur sur le port 80

 */
TServeur::TServeur() {
    // Enregister les fonctions pour gérer les chemins.
    Serveur.on("/", ServeurCertificat);
    Serveur.on("/capteurs", ServeurCapteurs);
    Serveur.on("/actionneurs", ServeurActionneurs);
    Serveur.on("/alerte", ServeurAlerte);
    Serveur.on("/type-jour", ServeurTypeJour);
    Serveur.on("/consignes", ServeurConsignes);
    Serveur.on("/cycle", ServeurCycle);

    Serveur.setServerKeyAndCert(CLE_PRIVEE, sizeof(CLE_PRIVEE), CERTIFICAT,
                                sizeof(CERTIFICAT));

    // Démarrer le serveur.
    Serveur.begin();
}
