/*!  @file                 TServeur.cpp
     @brief                Déclaration de TServeur

 */
//** **************************************************************************

#ifndef TServeur_h
#define TServeur_h

#include "../ITache/ITache.h"
#include <ESP8266WebServerSecure.h>
#include <ESP8266WiFi.h>

// Sérialize NaN à null plutôt qu'à NaN qui n'est pas compatible avec le
// standard JSON et plusieurs librairies de désérialization
#define ARDUINOJSON_ENABLE_NAN 0
#include <ArduinoJson.h>

//! Gère les communication avec l'application Android
class TServeur : public ITache {
private:
public:
    TServeur();
    int Executer();
};

#endif
