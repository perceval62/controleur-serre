#!/bin/bash

# Génère le fichier Cles.h

duree=$(expr 30 \* 365)
cle_privee="private.key"

# Générer la clé privée
openssl genrsa 512 > private.key
echo "static const uint8_t CLE_PRIVEE[] = R\"EOF(" > Cles.h
cat $cle_privee >> Cles.h
echo ")EOF\";" >> Cles.h
echo "" >> Cles.h


# Générer la clé publique
echo "static const uint8_t CERTIFICAT[] = R\"EOF(" >> Cles.h
openssl req -new -x509 -nodes -sha1 -days $duree -key $cle_privee >> Cles.h
echo ")EOF\";" >> Cles.h

# Supprimer le fichier de la clé privée
rm $cle_privee
