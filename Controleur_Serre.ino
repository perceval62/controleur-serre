/**
  Contrôleur de serre
  Name: Controleur_Serre
  Purpose:
  Target: NodeMCU v1.0

  @author TSO Cégep de l'Outaouais
  @version 0.1.0
*/

#include <DHTesp.h> //Bibliothèque pour les sondes de température et humidité
#include <ESP8266WiFi.h> //Bibliothèque pour le module WIFI

#include "AActionneur/AActionneur.h"
#include "ATachePeriodique/ATachePeriodique.cpp"
#include "Broches/Broches.h"
#include "ITache/ITache.h"
#include "TAsservissement/TAsservissement.cpp"
#include "TCapteur/TCapteur.cpp"
#include "TClientHistorique/TClientHistorique.cpp"
#include "TClientNotifications/TClientNotifications.cpp"
#include "TConnexionWiFi/TConnexionWiFi.cpp"
#include "TConsigne/TConsigne.h"
#include "TCycle/TCycle.h"
#include "TFournaise/TFournaise.cpp"
#include "TGroupeCapteurs/TGroupeCapteurs.cpp"
#include "TJour/TJour.h"
#include "TMoteur/TMoteur.cpp"
#include "TSelectionMoteur/TSelectionMoteur.cpp"
#include "TSynchroniseurTemps/TSynchroniseurTemps.cpp"
#include "TTacheCycle/TTacheCycle.cpp"
#include "TVentilateur/TVentilateur.cpp"
#include "Temps/Temps.cpp"

//---------------------------------------------------------------
//               Setup pour les paramètres Réseaux
//---------------------------------------------------------------

//! SSID du réseau WiFi
static const char *SSID = "Test Serre";
//! Mot de passe du réseau WiFi
static const char *MOT_DE_PASSE = "Test Serre";

TSelectionMoteur selectionMoteur(DIRECTION_MOTEURS, SELECTION_MOTEUR, 5.0);
TFournaise fournaise(CHAUFFAGE);
TVentilateur ventilateur(VENTILATION);
TGroupeCapteurs Capteurs(CAPTEURS, 5000);
TAsservissement Asservissement(5000);
TTacheCycle Cycle(5000);
TConnexionWiFi ConnexionWiFi(SSID, MOT_DE_PASSE, 3 * SECONDE);

TGroupeCapteurs groupeCapteurs(CAPTEURS, SECONDE * 2);

#include "TServeur/TServeur.cpp" // L'inclusion doit être faite après les
                                 // déclarations de variables globales,
                                 // car le serveur en a besoin
TServeur serveur;

void setup() {
    // Initialiser la communication sérielle.
    Serial.begin(115200); // Il faut désactiver la communication sérielle
                          // afin de pouvoir utiliser les broches D9 et D10
                          // (RX et TX)

    // Configurer les broches d'interrupteur en entrée.
    pinMode(SW_MODE, INPUT);
    pinMode(SW_DIRECTION_OUVRIR, INPUT);
    pinMode(SW_DIRECTION_FERMER, INPUT);
    pinMode(SW_SELECTION_MOTEUR, INPUT);
    pinMode(SW_CHAUFFAGE, INPUT);
    pinMode(SW_VENTILATION, INPUT);

    // Se connecter au WiFi.
    Serial.print("Connection à ");
    Serial.println(SSID);

    Asservissement.Fan = &ventilateur;
    Asservissement.Heater = &fournaise;
    Asservissement.MotorSelection = &selectionMoteur;
    Cycle.tfChauffage = &fournaise;
    Cycle.tvVentilation = &ventilateur;
}

void loop() {

    ConnexionWiFi.Executer();

    Lecture();
    if (digitalRead(SW_MODE) == MODE_MANUEL) {

        // Obtenir l'index du moteur à sélectionner.
        int indexMoteur =
            digitalRead(SW_SELECTION_MOTEUR) == MOTEUR1_SELECTIONNE;

        // Arrêter le moteur qui n'est pas sélectionné.
        selectionMoteur.Moteurs[!indexMoteur].Etat(dmNeutre);

        // Sélectionné le moteur à l'index.
        selectionMoteur.Etat(indexMoteur);

        // Obtenir la direction de moteurs sélectionnée.
        TDirectionMoteur direction = dmNeutre;
        if (digitalRead(SW_DIRECTION_OUVRIR) == DIRECTION_SELECTIONNE)
            direction = dmOuvrir;
        else if (digitalRead(SW_DIRECTION_FERMER) == DIRECTION_SELECTIONNE)
            direction = dmFermer;

        // Diriger le moteur sélectionné dans la direction sélectionnée.
        selectionMoteur.Moteurs[indexMoteur].Etat(direction);

        // Changer l'état du ventilateur en fonction de l'état de l'interrupteur
        // de ventilation.
        ventilateur.Etat(digitalRead(SW_VENTILATION) == ALLUMER);

        // Changer l'état de la fournaise en fonction de l'état de
        // l'interrupteur de chauffage.
        fournaise.Etat(digitalRead(SW_CHAUFFAGE) == ALLUMER);
    } else if (digitalRead(SW_MODE) == MODE_AUTOMATIQUE) {
        if (Cycle.Executer() == 0) {
            selectionMoteur.Executer();
            Asservissement.Executer();
        }
    }
    serveur.Executer();
}

void Lecture() {
    groupeCapteurs.Executer();
    Asservissement.Temperature = groupeCapteurs.SommaireTemperatureCourtTerme();
}
