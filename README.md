# Contrôleur de la serre
Ici se trouve le code pour le contrôleur de la serre. Il a été fait pour le [NodeMCU devkit v1.0](https://github.com/nodemcu/nodemcu-devkit-v1.0).  
Vous pouvez avoir accès à [la documentation du code générée par Doxygen ici](https://chapeau-melon/gitlab.io/controleur-serre/).

## Configuration de votre environnement
### Installer le Arduino IDE
Ce projet requiert le [Arduino IDE](https://www.arduino.cc/en/Main/Software#download) pour pouvoir être compilé.

### Installer le NodeMCU dans l'IDE
Commencez par ouvrir les préférences en faisant `Ctrl+,` et éditez le champ `Additionnal Board Manager URLs` ou `Gestionnaires de Cartes Supplémentaires` pour y ajouter `http://arduino.esp8266.com/stable/package_esp8266com_index.json`. Si ce champ n'était assurez vous qu'il y a une `,` après l'URL précédent.  
Ouvrez le gestionnaire de cartes à partir du menu en passant par `Outils > Carte > Gestionnaire de Cartes`. Entrez `esp8266` dans le champ en haut à droite et appuyé sur le bouton `Installer` de la librairie.

### Configurer le NodeMCU dans l'IDE
Sélectionnez `NodeMCU 1.0 (ESP-12E Module)` dans le menu `Outils > Carte`.  
Sélectionnez la plus haute vitesse possible dans `Outils > Vitesse de téléversement`. Si vous rencontrez des problèmes de téléversement vous pouvez toujours la modifier pour une vitesse plus basse.

### Installer les dépendances
Vous devez aussi installer les librairies desquelles dépendent ce projet:
* [DHTesp](https://github.com/beegee-tokyo/DHTesp)
* [ArduinoJson](https://github.com/bblanchon/ArduinoJson)

Pour les installer, ouvrez le gestionnaire de librairies de l'IDE en faisant `Ctrl+Shit+I`. Vous pouvez ensuite entrer le nom de la librairie à installer dans le champ en haut à droite, puis appuyer sur le bouton `Installer` en bas à droite de la librairie dont vous avez besoin.

### Téléverser le code dans le NodeMCU
Connecter le NodeMCU avec un câble `USB Micro B` à votre ordinateur et sélectionner le port auquel vous l'avez connecté dans `Outils > Port`.  
Appuyer sur le bouton téléverser dans la barre d'outils en haut à gauche.  
Une LED devrait clignoter sur votre carte pendant un certain temps et un terminal va s'ouvrir dans l'IDE et va afficher la progression du téléversement et vous afficher un message lorsqu'il sera terminé.

## [Tests](tests/README.md)
Un répertoire de tests se trouve dans le sous-dossier `test` de ce projet. Allez lire le README de cette section pour plus de détails sur leur odre d'exécution.

## Standard de formattage
Le standard pour le formattage du code dans ce projet est décrit dans le fichier `.clang-format`. Il s'applique à tous les fichiers de code, soient les fichiers `.h`, `.cpp` et `.ino`.  
Dans le dossier du projet, vouz pouvez exécuter la commande suivante pour reformatter tous les fichiers de code selon ce standard:
```sh
clang-format -i */*.cpp */*.h *.ino
```
Voici le [lien](http://releases.llvm.org/download.html) pour télécharger et installer [clang](http://clang.llvm.org/).
