/*!  @file                 TFournaise.h
     @brief                Déclaration de TFournaise

 */
//** **************************************************************************

#ifndef TFournaise_h
#define TFournaise_h

#include "../AActionneur/AActionneur.h"

//! Contrôle une fournaise pour le chauffage de la serre
class TFournaise : public AActionneur<bool> {
public:
    //! Broche de contrôle de la fournaise
    int Broche;

    TFournaise(int broche);

    using AActionneur<bool>::Etat;
    int Etat(bool etat);
};

#endif
