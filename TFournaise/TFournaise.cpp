/*!  @file                 TFournaise.cpp
     @brief                Implémentation de TFournaise

 */
//** **************************************************************************

#include "TFournaise.h"
#include <Arduino.h>

//! Niveau logique pour allumer la fournaise
#define ALLUME 1
//! Niveau logique pour éteindre la fournaise
#define ETEINT 0

//** ***************************************************************************
/*!  @brief                Initialise une fournaise avec la broche de contrôle
                           donnée et la ferme

     @return               Un code d'erreur
     @retval 0             Aucune erreur

 */
TFournaise::TFournaise(
    //! Broche de la fournaise
    int broche) {
    // Initialiser la broche à la broche donnée.
    Broche = broche;

    // Configurer la broche en sortie.
    pinMode(broche, OUTPUT);

    // Fermer la fournaise.
    Etat(false);
}

//** ***************************************************************************
/*!  @brief                Implémentation de AActionneur::Etat(etat)
                           Modifie l'état de la fournaise

     @return               Un code d'erreur
     @retval 0             Aucune erreur

 */
int TFournaise::Etat(
    //! Nouvel état de la fournaise
    bool etat) {
    // Changer l'état de la fournaise à celui donné.
    digitalWrite(Broche, !etat ? ALLUME : ETEINT); // TODO, inverser état est
                                                   // nécessaire pour le
                                                   // prototype, mais il faut
                                                   // absolument enlever cela
                                                   // pour la version finale qui
                                                   // n'en a pas besoin
    FEtat = etat;

    // Retourner le code de réussite à la routine appelante.
    return 0;
}
