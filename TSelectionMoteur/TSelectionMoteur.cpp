/*!  @file                 TSelectionMoteur.cpp
     @brief                Implémentation de TSelectionMoteur

 */
//** **************************************************************************

#include "TSelectionMoteur.h"
#include <Arduino.h>

//** ***************************************************************************
/*!  @brief                Créé un gestionnaire de sélection de moteur et des
                           moteurs avec les mêmes broches de direction données
                           et sélectionne le premier moteur

*/
TSelectionMoteur::TSelectionMoteur(
    //! Broches de contrôle de la direction des moteurs
    const int *brochesDirection,
    //! Broche pour sélectionner le moteur
    int brocheSelection,
    //! Hystérésis de la position moteurs
    double hysteresis) :
    BrocheSelection(brocheSelection),
    Moteurs({TMoteur(hysteresis, brochesDirection),
             TMoteur(hysteresis, brochesDirection)}) {
    // Configurer la broche de sélection en sortie.
    pinMode(brocheSelection, OUTPUT);

    // Sélectionner le premier moteur.
    Etat(0);
}

//** ***************************************************************************
/*!  @brief                Implémentation de int AActionneur::Etat(TEtat)
                           Changé l'index du moteur sélectionné

     @return               Un code d'erreur
     @retval 0             Aucune erreur
     @retval 1             Index invalide

*/
int TSelectionMoteur::Etat(
    //! Index du moteur à sélectionner
    unsigned int moteur) {
    if (moteur == 0) {
        digitalWrite(BrocheSelection, LOW);
        FEtat = moteur;
    } else if (moteur == 1) {
        digitalWrite(BrocheSelection, HIGH);
        FEtat = moteur;
    }
}

//** ***************************************************************************
/*!  @brief                Implémentation de int ITache::Executer()
                           Déplace le moteur sélectionné. Si le moteur
                           sélectionné a fini son déplacement, sélectionne
                           l'autre moteur

     @return               Un code d'erreur
     @retval 0             Aucune erreur

     @internal
     @todo                 Si aucun des deux moteurs n'est en mouvement, il est
                           mieux de sélectionner le moteur 0, puisque ça ne
                           consomme aucun courant de l'avoir sélectionner, alors
                           que sélectionner le moteur 1 consomme ~80mA

 */
int TSelectionMoteur::Executer() {
    // Obtenir le moteur sélectionné.
    TMoteur &moteur = Moteurs[FEtat];

    // Exécuter le moteur sélectionné.
    // Si le moteur n'est pas en mouvement
    if (moteur.Executer() == 0) {
        // Obtenir l'autre moteur et son index.
        int indexAutreMoteur = FEtat == 0 ? 1 : 0;
        TMoteur &autreMoteur = Moteurs[indexAutreMoteur];

        // Exécuter l'autre moteur.
        // Si l'autre moteur se met en mouvement
        if (autreMoteur.Executer() != 0) {
            // Selectionner l'autre moteur.
            Etat(indexAutreMoteur);
        }
    }
}
