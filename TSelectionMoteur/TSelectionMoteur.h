/*!  @file                 TSelectionMoteur.h
     @brief                Déclaration de TSelectionMoteur

 */
//** **************************************************************************

#ifndef TSelectionMoteur_h
#define TSelectionMoteur_h

#include "../AActionneur/AActionneur.h"
#include "../ITache/ITache.h"
#include "../TMoteur/TMoteur.h"

//! Nombre de moteurs dans la sélection
#define NB_MOTEURS 2

//! Gère la sélection du moteur à déplacer
class TSelectionMoteur : public ITache, public AActionneur<unsigned int> {
public:
    //! Broche pour sélectionné le moteur
    int BrocheSelection;
    //! Moteurs parmi lesquels choisir
    TMoteur Moteurs[NB_MOTEURS];

    TSelectionMoteur(const int *brochesDirection, int brocheSelection,
                     double hysteresis);

    using AActionneur<unsigned int>::Etat;
    int Etat(unsigned int moteur);

    int Executer();
};

#endif
