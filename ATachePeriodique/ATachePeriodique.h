/*!  @file                 ATachePeriodique.h
     @brief                Déclaration de ATachePeriodique

 */
//** **************************************************************************

#ifndef ATachePeriodique_h
#define ATachePeriodique_h

#include "../ITache/ITache.h"
#include "../Temps/Temps.h"

//! Classe abstraite qui implémente ITache pour exécuter une tâche à un interval
//! donné
class ATachePeriodique : public ITache {
public:
    //! Interval auquel là tâche périodique doit être éxécutée
    TTemps Periode;

    ATachePeriodique(TTemps periode);

    int Executer();

private:
    //! Heure à laquelle la tâche a été exécutée pour la dernière fois
    TTemps FDerniereExecution;

protected:
    //*************************************************************************
    /*!  @brief                Tâche à exécuter périodiquement

         @return               Un code d'erreur
         @retval 0             Aucune erreur
         @retval autre         Voir l'erreur de l'implémentation

     */
    virtual int ExecuterPeriodique() = 0;
};

#endif
