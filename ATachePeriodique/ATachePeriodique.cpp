/*!  @file                 ATachePeriodique.cpp
     @brief                Implémentation de ATachePeriodique

 */
//** **************************************************************************

#include "ATachePeriodique.h"

//** ***************************************************************************
/*!  @brief                Initialise la tâche avec une période donnée

 */
ATachePeriodique::ATachePeriodique(
    //! Interval d'exécution de la tâche périodique
    TTemps periode) {
    Periode = periode;
    FDerniereExecution = 0;
}

//** ***************************************************************************
/*!  @brief                Initialise la tâche avec une période donnée

     @return               Un code d'erreur
     @retval 0             Aucune erreur
     @retval autre         Voir l'erreur retournée par l'implémentation de
                           ExecuterPeriodique

 */
int ATachePeriodique::Executer() {
    int codeErreur = 0;

    // Si la période est dépassée
    if (temps() - FDerniereExecution > Periode) {
        // Exécuter la tâche périodique.
        codeErreur = ExecuterPeriodique();

        // Assigner l'heure actuelle à l'heure de la dernière exécution.
        FDerniereExecution = temps();
    }

    return codeErreur;
}
