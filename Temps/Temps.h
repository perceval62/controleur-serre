/*!  @file                 Temps.h
     @brief                Déclaration de TTemps et temps

     @internal

     @todo                 Créer une documentation avec des exemples pour ce
                           module
 */
//** **************************************************************************

#ifndef Temps_h
#define Temps_h

// configTime
#include <Arduino.h>
// time
#include <time.h>

//! Temps dans le RTC du NodeMCU. Voir https://en.wikipedia.org/wiki/Unix_time
typedef uint32_t TTemps;

//! Obtenir l'heure
#define temps() time(nullptr)

void nonBlockDelay(unsigned long length);

struct callbackReturnType {
    void *data;
    struct callbackReturnType *nextReturnValue;
    int isLastItem;
};

//**===============================================================================
/**
 * @brief   delay function that executes a routine while waiting
 *
 * @param   callbackFunc: pointeur vers une fonction de signature "void *
 * fn(void*);"
 * @param   args pointeur vers les donnees a passer a la routine callbackFunc
 * @param   le temps a boucler.
 *
 * @return  une liste chainee unidirectionnelle de struct contenant chaques
 * retours de la routine.
 */
struct callbackReturnType *asyncDelay(void *(*callbackFunc)(void *), void *args,
                                      unsigned long length);

//**===============================================================================
/**
 * @brief   Routine pour libere la memoire utilisee par la liste chainee
 * retournee
 *
 * @param   list: pointeur vers une liste chainee de type callbackReturnType
 *
 * @return  le nombre d'elements liberes.
 *
 */
int free_callbackReturnType(struct callbackReturnType *list);

//================================================================
// Constantes de temps
//================================================================
//! Valeur de TTemps pour une heure
#define HEURE 3600
//! Valeur de TTemps pour une minute
#define MINUTE 60
//! Valeur de TTemps pour une seconde
#define SECONDE 1

#endif
