/*!  @file                 Temps.h
     @brief                Implémentation de temps

 */
//** **************************************************************************

#include "Temps.h"
#include "Arduino.h"

//==============================================================================
// Implémentation de nonBlockDelay
//==============================================================================

/**
 * @brief   Routine d'attente non bloquante. Elle n'empeche pas
 *          l'execution d'ISRs.
 *
 **/
void nonBlockDelay(unsigned long length) {
    /* obtenir le temps present */
    unsigned long startTime = millis();

    /* Tant que la consigne de temps n'est pas atteinte */
    while (millis() - startTime < length) {
        /* Ne rien faire */
    }
}

/**
 * @brief   Routine d'attente non bloquante. Elle n'empeche pas
 *          l'execution d'ISRs. De plus, elle execute une routine
 *          lors de l'attente, d'ou le nom "active"
 *
 * @return  Un pointeur vers une liste de valeures de retours
 *
 * @note    Le ownership des donnees retournees est dans les mains
 *          de l'appellant. La routine ne fera aucun cleanup...
 *
 **/

struct callbackReturnType *asyncDelay(void *(*callbackFunc)(void *), void *args,
                                      unsigned long length) {
    /* NULL checks*/
    if (callbackFunc == NULL)
        if (args == NULL) {
            return (NULL);
        }

    /* Allouer le premier element de la liste de retours */
    struct callbackReturnType *firstElement =
        (struct callbackReturnType *)malloc(sizeof(struct callbackReturnType));
    /* Creer un iterateur pour conserver l'adresse du premier element de la
     * liste */
    struct callbackReturnType *iter = firstElement;

    unsigned long startTime = millis();
    /* Tant que la consigne de temps n'est pas atteinte */
    while (millis() - startTime < length) {
        // appeller la routine passee en parametres.
        iter->data = callbackFunc(args); // Obtenir un pointeur vers son
                                         // retours.

        // Allouer une nouvelle adresse memoire.
        iter->nextReturnValue = (struct callbackReturnType *)malloc(
            sizeof(struct callbackReturnType));

        /* Si on est encore dans la boucle, cet item nest pas le node de fin */
        iter->isLastItem = 0;

        // Apporter notre iterateur vers cette prochaine espace memoire
        iter = iter->nextReturnValue;
    }

    iter->isLastItem = 1;
    // Lorsque la boucle ce termine, nous avons une struct vide allouee en
    // memoire. Il faut la liberer
    free(iter);

    return firstElement;
}

/* Cleanup */
/* Routine pour liberer la liste retournee par asyncDelay() */

/**
 * @parametresune liste de type callbackReturnType
 *
 * @ return nombre d'elements liberes */
int free_callbackReturnType(struct callbackReturnType *list) {
    /* Null check */
    if (list == NULL) {
        return (0);
    }
    struct callbackReturnType *iter = list;
    struct callbackReturnType *previousItemIter;
    unsigned long numElements = 0;
    /* Tant que il y a des elements dans la liste */
    while (iter->isLastItem < 1) {
        /* point previous element iterator to current element */
        previousItemIter = iter;
        /* point first iterator to next element*/
        iter = iter->nextReturnValue;
        /* delete previouse element*/
        free(previousItemIter);
        numElements++;
    }
    /* Delete le dernier node de la liste, il devrait etre vide et le flag
     * isLastItem a 1 */
    free(iter);
    return (numElements);
}
