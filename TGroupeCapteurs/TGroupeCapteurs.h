/*!  @file                 TGroupeCapteurs.h
     @brief                Déclaration de TGroupeCapteurs

 */
//** **************************************************************************

#ifndef TGroupeCapteurs_h
#define TGroupeCapteurs_h

#include "../ATachePeriodique/ATachePeriodique.h"
#include "../TCapteur/TCapteur.h"
#include <vector>

//! Nombre de capteurs dans un groupe de capteurs
#define NB_CAPTEURS 2

//! Possède tous les capteurs, gère leur exécution et fait le sommaire de leurs
//! résultats
class TGroupeCapteurs : public ATachePeriodique {
public:
    //! Capteurs du groupe
    TCapteur Capteurs[NB_CAPTEURS];

    TGroupeCapteurs(const int *broches,
                    long periode); // TODO ajouter une période par défaut

    TSommaireMesures SommaireTemperatureCourtTerme();
    TSommaireMesures SommaireHumiditeCourtTerme();

protected:
    //! Lit les valeurs des capteurs
    int ExecuterPeriodique();
};

#endif
