/*!  @file                 TGroupeCapteurs.cpp
     @brief                Implémentation de TGroupeCapteurs

 */
//** **************************************************************************

#include "TGroupeCapteurs.h"

//** ***************************************************************************
/*!  @brief                Initialise le groupe de capteur avec les broches
                           de capteurs et la période donnée

 */
TGroupeCapteurs::TGroupeCapteurs(
    //! Pointeur vers la liste de broches pour les capteurs
    const int *broches,
    //! Interval de lecture des capteurs
    long periode) :
    ATachePeriodique(periode) {
    for (int i = 0; i < NB_CAPTEURS; i++)
        Capteurs[i] = TCapteur(broches[i]);
}

//** ***************************************************************************
/*!  @brief                Combine le sommaire de température de tous les
                           capteurs

     @return               le sommaire des température à court terme

 */
TSommaireMesures TGroupeCapteurs::SommaireTemperatureCourtTerme() {
    TSommaireMesures sommaires[NB_CAPTEURS];

    // Pour chaque capteur
    for (int i = 0; i < NB_CAPTEURS; i++)
        // Obtenir le sommaire de température à court terme.
        sommaires[i] = Capteurs[i].TemperatureCourtTerme.Sommaire();

    // Combiner tous les sommaires et retourner le résultat.
    return TSommaireMesures(sommaires, NB_CAPTEURS);
}

//** ***************************************************************************
/*!  @brief                Combine le sommaire d'humidité de tous les capteurs

     @return               Le sommaire d'humidité à court terme

 */
TSommaireMesures TGroupeCapteurs::SommaireHumiditeCourtTerme() {
    TSommaireMesures sommaires[NB_CAPTEURS];

    // Pour chaque capteur
    for (int i = 0; i < NB_CAPTEURS; i++)
        // Obtenir le sommaire d'humidité à court terme.
        sommaires[i] = Capteurs[i].HumiditeCourtTerme.Sommaire();

    // Combiner tous les sommaires et retourner le résultat.
    return TSommaireMesures(sommaires, NB_CAPTEURS);
}

//** ***************************************************************************
/*!  @brief                Lit les valeurs des capteurs

     @return               Un code d'erreur
     @retval 0             Aucune erreur
     @retval 1             Aucun capteur

 */
int TGroupeCapteurs::ExecuterPeriodique() {
    // Pour chaque capteur
    for (TCapteur &capteur : Capteurs)
        // Lire la température et l'humidité du capteur.
        capteur.Lire();
}
