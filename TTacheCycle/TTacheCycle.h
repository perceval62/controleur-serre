/*!  @file                 TTacheCycle.h
     @brief                Déclaration de TTacheCycle et TEtapeCycle

 */
//** **************************************************************************

#ifndef TTacheCycle_h
#define TTacheCycle_h

#include "../ATachePeriodique/ATachePeriodique.h"
#include "../TCycle/TCycle.h"

//! Étape dans un cycle
enum TEtapeCycle {
    //! Chauffage de la serre
    ecChauffage,
    //! Ventilation de la serre
    ecVentilation,

    ecPremier = ecChauffage,
    ecDernier = ecVentilation,
};

//! Gère l'exécution d'un cycle
class TTacheCycle : public ATachePeriodique {
private:
    //! Nombre de fois qu'un cycle s'est répété
    int FRepetition;
    //! Étape à laquelle on est rendu dans le cycle
    TEtapeCycle Etape;
    //! Heure à laquelle la répétition a commencé
    long DebutEtape;
    //! Dernière fois que le cycle à été répété
    int DerniereRepetition;

public:
    //! Cycle à exécuter
    TCycle Cycle;
    TFournaise *tfChauffage;
    TVentilateur *tvVentilation;
    TTacheCycle(long periode); // TODO Ajouter une période par défaut
protected:
    int ExecuterPeriodique();
};

#endif
