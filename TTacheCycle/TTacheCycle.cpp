/*!  @file                 TTacheCycle.cpp
     @brief                Implémentation de TTacheCycle

 */
//** **************************************************************************

#include "TTacheCycle.h"
#include <Arduino.h>

//! Niveau logique pour allumer le ventilateur ou la fournaise
#define ALLUME 1
//! Niveau logique pour éteindre le ventilateur ou la fournaise
#define ETEINT 0

//** ***************************************************************************
/*!  @brief                Initialise une tâche avec la période donnée et
                           un cycle avec 0 répétitions

 */
TTacheCycle::TTacheCycle(
    //! Interval d'asservissement du cycle
    long periode) :
    ATachePeriodique(periode) {
    FRepetition = 0;
    DerniereRepetition = 0;
}

/*!  @brief                Implémentation de
                           ATachePeriodique::ExecuterPeriodique

     @return               Un code d'erreur
     @retval 0             Aucune erreur
     @retval 1             En fonction
     @retval 2             Chauffage
     @retval 3             Ventilation

 */
int TTacheCycle::ExecuterPeriodique() {
    int CodeErreur = 0;
    if (Cycle.Repetitions > 0) {
        CodeErreur = 1;
        if (FRepetition == Cycle.Repetitions) {
            FRepetition = 0;
            DerniereRepetition = 0;
        }
        if (FRepetition == DerniereRepetition + 1) {
            DebutEtape = millis();
        }
        long Tempsecoule = millis() - DebutEtape;
        if (Tempsecoule <= Cycle.DureeChauffage) {
            Etape = ecChauffage;
            tfChauffage->Etat(ALLUME);
            CodeErreur = 2;
        } else if (Tempsecoule <=
                   (Cycle.DureeVentilation + Cycle.DureeChauffage)) {
            tfChauffage->Etat(ETEINT);
            tvVentilation->Etat(ALLUME);
            Etape = ecVentilation;
            CodeErreur = 3;
        } else if (Tempsecoule >
                   (Cycle.DureeVentilation + Cycle.DureeChauffage)) {
            tvVentilation->Etat(ETEINT);
            DerniereRepetition = FRepetition;
            FRepetition++;
        }
    }
}
