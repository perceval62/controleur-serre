/*!  @file                 TConnexionWiFi.cpp
     @brief                Implémentation de la classe TConnexionWiFi.

 */
//** **************************************************************************

#include "TConnexionWiFi.h"

//** ***************************************************************************
/*!  @brief                initialise le nom du routeur, le mot de passe et le
                            le temps entre chaque demande pour la connexion WiFi

 */
TConnexionWiFi::TConnexionWiFi(
    //! SSID du Routeur.
    const char *ssid,
    //! Mot de passe du routeur.
    const char *motDePasse,
    //! Temps entre chaque demande pour la connexion au routeur.
    unsigned int periode) :
    ATachePeriodique(periode) {

    FSSID = ssid;
    FMotDePasse = motDePasse;

    // Initialiser à non envoyer.
    FIPEnvoye = false;

    // Initialiser le nombre de demande à 0.
    FNbDemande = 0;
}

//** ***************************************************************************
/*!   @brief                Tache à faire periodiquement

      @return               Un code d'erreur
      @retval 0             Aucune erreur
      @retval autre         @todo ajouter des code erreurs

 */
int TConnexionWiFi::ExecuterPeriodique() {

    int codeErreur;

    // Si le wifi n'est pas connecté.
    if (WiFi.status() != WL_CONNECTED) {

        Serial.println("Tentative de connexion...");
        WiFi.begin(FSSID, FMotDePasse);
        codeErreur = 0;
    }

    // Sinon afficher l'adress IP sur le pour sériel si ce n'est pas déja fait.
    else if (FIPEnvoye == false) {
        Serial.println("Adresse IP: ");
        Serial.println(WiFi.localIP());
        FIPEnvoye = true;
        codeErreur = 0;
    }

    // Si le contrôleur n'est pas connecté et n'a pas fait plus de 3 demande.
    if (WiFi.status() == WL_DISCONNECTED && FNbDemande < 3) {
        FNbDemande++;
        FIPEnvoye = false;
        codeErreur = 0;
    }
    // Sinon si le contrôleur n'est pas connecté après trois demandes.
    else if (WiFi.status() != WL_CONNECTED) {
        FNbDemande = 0;
        FIPEnvoye = false;
        WiFi.disconnect();
        Serial.println("Reconnexion..");
        codeErreur = 0;
    }

    return codeErreur;
}
