/*!  @file                 TConnexionWiFi.h
     @brief                Déclaration de classe TConnexionWiFi.

 */
//** **************************************************************************

#ifndef TCONNEXIONWIFI_H
#define TCONNEXIONWIFI_H

#include "../ATachePeriodique/ATachePeriodique.h"

//! Connecte le contrôleur au WiFi lors qu'il est déconnecté.
class TConnexionWiFi : public ATachePeriodique {

private:
    //! Nom du reseau.
    const char *FSSID;
    //! Mot de passe du reseau.
    const char *FMotDePasse;
    //! Compteur pour les problemes de connexion.
    unsigned int FNbDemande;
    //! État de l'envois de l'adresse IP sur le port sériel.
    bool FIPEnvoye;

public:
    TConnexionWiFi(const char *, const char *, TTemps);

    int ExecuterPeriodique();
};

#endif
