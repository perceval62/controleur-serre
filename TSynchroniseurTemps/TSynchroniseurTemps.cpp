/*!  @file                 TSynchroniseurTemps.cpp
     @brief                Implémentation de TSynchroniseurTemps

 */
//** **************************************************************************

#include "TSynchroniseurTemps.h"

//! Serveur NTP #1
static const char SERVEUR_NTP_1[] = "ca.pool.ntp.org";
//! Serveur NTP #2 (utilisé si le #1 ne fonctionne pas)
static const char SERVEUR_NTP_2[] = "1.ca.pool.ntp.org";
//! Serveur NTP #3 (utilisé si les #1 et 2 ne fonctionnent pas)
static const char SERVEUR_NTP_3[] = "2.ca.pool.ntp.org";

//** ***************************************************************************
/*!  @brief                Initialise le synchroniseur de temps avec l'interval
                           de synchronisation donné

     @return               Un code d'erreur
     @retval 0             Aucune erreur

 */
TSynchroniseurTemps::TSynchroniseurTemps(
    //! Interval de synchronisation de l'heure
    TTemps periode) :
    ATachePeriodique(periode) {
    FuseauHoraire = -5 * 3600;
}

//** ***************************************************************************
/*!  @brief                Implémentation de
                           ATachePeriodique::ExecuterPeriodique
                           Synchronise le RTC avec le protocole NTP

     @return               Un code d'erreur
     @retval 0             Aucune erreur

 */
int TSynchroniseurTemps::ExecuterPeriodique() {
    int dst = 3600;
    // Synchroniser le temps sur le nodeMCU avec l'heure du Serveur NTP
    configTime(FuseauHoraire, dst, SERVEUR_NTP_1, SERVEUR_NTP_2, SERVEUR_NTP_3);
    /*
      Pour obtenir la date l'usage du code ci-dessous est nécéssaire.
      Ex.
      time_t now = time(nullptr);
      struct tm* p_tm = localtime(&now);
      struct tm contient
      tm_mday // Jours
      tm_mon  // Mois
      tm_year // Année
      tm_hour // Heures
      tm_min  // Minutes
      tm_sec  // Secondes

    */
}
