/*!  @file                 TSynchroniseurTemps.h
     @brief                Déclaration de TSynchroniseurTemps

 */
//** **************************************************************************

#ifndef TSynchroniseurTemps_h
#define TSynchroniseurTemps_h

#include "../ATachePeriodique/ATachePeriodique.h"

//! Synchronise le RTC avec le protocole NTP à un interval donné
class TSynchroniseurTemps : public ATachePeriodique {
private:
    //! Fuseau horaire
    int FuseauHoraire;

public:
    TSynchroniseurTemps(
        TTemps periode); // TODO ajouter une valeur par défaut de 1 jour
protected:
    int ExecuterPeriodique();
};

#endif
